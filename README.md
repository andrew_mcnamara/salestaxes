# Sales Tax
***

Generating a receipt for items in a shopping basket, including calculating sales taxes.


## Assumptions

***

 - ruby version 2.0
 - RVM for ruby version management
 - Bundler for Gem
 - To determine if product was imported a simple string match on word 'imported'
 - To determine the type of product use simple string match on words like 'book' or 'pill'
 - No file reading was included - as it is assumed this program would be driven from RSPEC tests
 - Receipts will be printed to STDOUT
 - Basic tax rate 10%
 - Import duty tax rate 5%

## How to run

***

 - cd into the the 'sales_taxes' folder
 - rvm install ruby-2.0.0-p451
 - rvm --create use ruby-2.0.0-p451@sales_taxes
 - gem install bundler
 - bundle
 - rspec

## Program Structure

***

 - SalesTax - the pseudo main class - invoke method print_receipt with a shopping basket to print a receipt for a shopping basket
 - ReceiptGenerator - generates a receipt for a shopping basket
 - Receipt - this holds the details of the receipt - i.e. total and sales tax
 - LineItem - a line in the receipt report
 - ShoppingBasketProcessor - Takes a string representing a shopping basket and converts into a list of LineItems
 - SalesTaxCalculator - is responsible for calculating the sales tax of items
 - ReceiptPrinter - responsible for printing a receipt