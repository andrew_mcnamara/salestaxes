class SalesTaxCalculator

  attr_reader :basic_tax, :import_tax

  def initialize(attrs = {})
    @basic_tax = attrs.fetch(:basic_tax) { 10.0 }
    @import_tax = attrs.fetch(:import_tax) { 5.0 }
    @exempt_categories = %i{food medical books}
  end

  def calculate_tax(line_item)
    tax = 0
    tax = apply_tax_rate(basic_tax, line_item) unless is_exempt_from_basic_tax?(line_item)
    tax = tax + apply_tax_rate(import_tax, line_item) if line_item.is_imported?
    tax
  end

  def apply_tax_rate(tax_rate, line_item)
    round_tax_to_nearest_five(line_item.price * (tax_rate/100.0))
  end

  private

  def round_tax_to_nearest_five(tax)
    (tax * 20.0).ceil / 20.00
  end

  def is_exempt_from_basic_tax?(line_item)
    @exempt_categories.include?(line_item.category)
  end
end
