class LineItem
  CATEGORY_MAP= {medical: /pill/i,
                 books: /book/i,
                 food: /chocolate/i}

  attr_reader :product, :quantity, :price, :imported
  attr_accessor :sales_tax

  def initialize(attrs = {})
    @product = attrs.fetch(:product)
    @quantity = attrs.fetch(:quantity).to_i
    @price = attrs.fetch(:price).to_f
    @sales_tax = attrs.fetch(:sales_tax, 0.00).to_f
    @imported = attrs.fetch(:imported)
  end

  def price_with_tax
    price + sales_tax
  end

  def category
    CATEGORY_MAP.select { |name, predicate| product =~ predicate }.keys.first
  end

  def is_imported?
    imported
  end

  def description
    "#{(is_imported?) ? 'imported ' : ''}#{product}".gsub(/\s+/, ' ')
  end

  def ==(other_item)
    %w{product quantity price sales_tax imported }.all? { |attr| self.send(attr)== other_item.send(attr) }
  end

end