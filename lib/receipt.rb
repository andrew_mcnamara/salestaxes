class Receipt
  attr_reader :line_items, :total, :sales_tax

  def initialize(attrs = {})
    @line_items = attrs.fetch(:line_items)
    @total = attrs.fetch(:total)
    @sales_tax = attrs.fetch(:sales_tax)
  end
end



