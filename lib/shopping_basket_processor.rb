class ShoppingBasketProcessor
  attr_reader :sales_tax_calculator

  def initialize(sales_tax_calulator = SalesTaxCalculator.new)
    @sales_tax_calculator = sales_tax_calulator
  end

  def process(shopping_basket)
    strip_headers(shopping_basket).map do |line|
      process_line(line)
    end
  end

  def strip_headers(shopping_basket)
    shopping_basket.split("\n").reject { |line| line =~ /Quantity/ }
  end

  private
  def process_line(line)
    line_item_details = line.split(/\s*,\s*/)
    quantity, product, price = line_item_details
    imported = is_imported?(product)
    product = product.gsub(/\s*imported\s*/, ' ')
    LineItem.new(quantity: quantity,
                 product: product,
                 price: price,
                 imported: imported).tap { |line_item| apply_sales_tax(line_item) }
  end

  def is_imported?(product)
    product.include?("imported")
  end

  def apply_sales_tax(line_item)
    line_item.sales_tax = sales_tax_calculator.calculate_tax(line_item)
  end
end