class ReceiptPrinter
  def print(receipt)
    receipt.line_items.each do |line_item|
      puts "#{line_item.quantity}, #{line_item.description}, #{format("%.2f", line_item.price_with_tax)}"
    end
    puts "\nSales Taxes: #{format("%.2f", receipt.sales_tax)}"
    puts "Total: #{format("%.2f", receipt.total)}"
  end
end