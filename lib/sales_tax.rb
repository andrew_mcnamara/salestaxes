class SalesTax
  def initialize(receipt_generator = ReceiptGenerator.new, receipt_printer = ReceiptPrinter.new)
    @receipt_generator = receipt_generator
    @receipt_printer = receipt_printer
  end

  def print_receipt(for_shopping_basket)
    receipt = receipt_generator.generate_receipt(for_shopping_basket)
    receipt_printer.print(receipt)
  end

  private
  def receipt_printer
    @receipt_printer
  end

  def receipt_generator
    @receipt_generator
  end
end