class ReceiptGenerator
  attr_reader :shopping_basket_processor

  def initialize(shopping_basket_processor = ShoppingBasketProcessor.new)
    @shopping_basket_processor = shopping_basket_processor
  end

  def generate_receipt(for_basket)
    line_items = shopping_basket_processor.process(for_basket)
    Receipt.new(line_items: line_items, total: total(line_items), sales_tax: total_sales_tax(line_items))
  end

  def total(line_items)
    line_items.map(&:price_with_tax).inject(:+)
  end

  def total_sales_tax(line_items)
    line_items.map(&:sales_tax).inject(:+)
  end

end