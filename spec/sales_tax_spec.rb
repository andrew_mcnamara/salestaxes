require 'spec_helper'

shared_examples 'Calculating sales taxes for items in a shopping basket' do
  let(:sales_tax) { SalesTax.new }
  it 'generates an receipt for all items' do
    capture_std_out = StringIO.new
    $stdout = capture_std_out
    sales_tax.print_receipt(shopping_basket)
    expect(capture_std_out.string).to eql(expected_receipt)
  end
end

describe "Shopping basket 'input1'" do
it_behaves_like 'Calculating sales taxes for items in a shopping basket' do
   let(:shopping_basket) { <<END_SHOPPING_BASKET
Quantity, Product, Price
1, book, 12.49
1, music CD, 14.99
1, chocolate bar, 0.85
END_SHOPPING_BASKET
   }
   let(:expected_receipt) { <<RECEIPT_DETAILS
1, book, 12.49
1, music CD, 16.49
1, chocolate bar, 0.85

Sales Taxes: 1.50
Total: 29.83
RECEIPT_DETAILS
   }
end
end

describe "Shopping basket 'input2'" do
  it_behaves_like 'Calculating sales taxes for items in a shopping basket' do
    let(:shopping_basket) { <<END_SHOPPING_BASKET
Quantity, Product, Price
1, imported box of chocolates, 10.00
1, imported bottle of perfume, 47.50
END_SHOPPING_BASKET
    }
    let(:expected_receipt) { <<RECEIPT_DETAILS
1, imported box of chocolates, 10.50
1, imported bottle of perfume, 54.65

Sales Taxes: 7.65
Total: 65.15
RECEIPT_DETAILS
    }
  end
end

describe "Shopping basket 'input3'" do
  it_behaves_like 'Calculating sales taxes for items in a shopping basket' do
    let(:shopping_basket) { <<END_SHOPPING_BASKET
Quantity, Product, Price
1, imported bottle of perfume, 27.99
1, bottle of perfume, 18.99
1, packet of headache pills, 9.75
1, box of imported chocolates, 11.25
END_SHOPPING_BASKET
    }
    let(:expected_receipt) { <<RECEIPT_DETAILS
1, imported bottle of perfume, 32.19
1, bottle of perfume, 20.89
1, packet of headache pills, 9.75
1, imported box of chocolates, 11.85

Sales Taxes: 6.70
Total: 74.68
RECEIPT_DETAILS
    }
  end
end
