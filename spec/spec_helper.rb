require_relative '../support/factory_girl'

%w{shopping_basket_processor
line_item receipt_generator
receipt_printer
receipt sales_tax
sales_tax_calculator }.each { |lib_file| require_relative "../lib/#{lib_file}" }