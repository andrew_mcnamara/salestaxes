describe 'Calculating sales tax for an item' do
  let(:sales_tax_calculator) { SalesTaxCalculator.new({basic_tax: 5, import_tax: 5}) }
  context 'an basic tax exempt item' do
    context 'that is not imported' do
      let(:non_imported_exempt_item) { build(:line_item, price: 10, product: 'book', imported: false) }
      it 'has no basic sales tax' do
        expect(sales_tax_calculator.calculate_tax(non_imported_exempt_item)).to eq(0.0)
      end
    end
    context 'that is imported' do
      let(:imported_exempt_item) { build(:line_item, price: 11.25, product: 'box of imported chocolates', imported: true) }
      it 'has an import tax applied' do
        expect(sales_tax_calculator.calculate_tax(imported_exempt_item)).to eq(0.60)
      end
    end
  end
  context 'an nonexempt item' do
    let(:non_exempt_imported_line_item) { build(:line_item, price: 10, product: 'imported item', imported: true) }

    context 'that is imported' do
      it 'has import tax and basic sales tax applied' do
        expect(sales_tax_calculator.calculate_tax(non_exempt_imported_line_item)).to eq(1.0)
      end
    end
  end
end