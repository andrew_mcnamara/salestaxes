require 'spec_helper'

describe 'Generating a receipt for items in shopping basket' do
  let(:shopping_basket) { <<END_SHOPPING_BASKET
Quantity, Product, Price
1,  music CD, 14.99
END_SHOPPING_BASKET
  }

  let (:receipt_generator) { ReceiptGenerator.new }
  let(:expected_line_item) { build(:line_item, quantity: 1, product: 'music CD', price: 14.99, imported: false, sales_tax: 1.5) }


  it 'adds line items for each product in the basket' do
    receipt = receipt_generator.generate_receipt(shopping_basket)
    expect(receipt.line_items).to include(expected_line_item)
  end

  it 'calculates the total for all the line items' do
    receipt = receipt_generator.generate_receipt(shopping_basket)
    expect(receipt.total.round(2)).to eql(16.49)
  end

  it 'calculates the sales tax for all of the line items' do
    receipt = receipt_generator.generate_receipt(shopping_basket)
    expect(receipt.sales_tax).to eql(1.50)
  end
end