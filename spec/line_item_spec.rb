require 'spec_helper'
describe 'A line item in a receipt' do
  let(:expected_line_item) { build(:line_item, quantity: 2, product: 'some item', price: 1.0, imported: true, sales_tax: 1.00) }

  it 'can be initialized from a hash' do
    test_item = LineItem.new({product: 'some item', price: 1.0, quantity: 2, imported: true, sales_tax: 1.00})
    expect(test_item).to eq(expected_line_item)
  end

  describe 'Determining if item is imported' do
    context 'for an imported item' do
      let(:line_item) { build(:line_item, imported: true) }
      it 'is_imported? returns true' do
        expect(line_item.is_imported?).to be_true
      end
    end

    context 'for an non imported item' do
      let(:line_item) { build(:line_item, imported: false) }
      it 'is_imported? returns true' do
        expect(line_item.is_imported?).to be_false
      end
    end
  end

  describe 'Determining the category for products' do
    let(:book_line_item) { build(:line_item, product: 'book') }
    let(:medical_line_item) { build(:line_item, product: 'pill') }
    let(:food_line_item) { build(:line_item, product: 'chocolate') }
    it 'categorizes books' do
      expect(book_line_item.category).to eq(:books)
    end
    it 'categorizes medical items' do
      expect(medical_line_item.category).to eq(:medical)
    end

    it 'categorizes food items' do
      expect(food_line_item.category).to eq(:food)
    end
  end

  describe 'Provides an item description' do
    context 'for an imported item' do
      let(:line_item) { build(:line_item, imported: true) }
      it 'prepends the product name with imported' do
        expect(line_item.description).to eql('imported book')
      end
    end

    context 'for an non imported item' do
      let(:line_item) { build(:line_item, imported: false) }
      it 'returns the product name for the description' do
        expect(line_item.description).to eql('book')
      end
    end
  end
end
