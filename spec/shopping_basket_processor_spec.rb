require 'spec_helper'
describe 'Processing a shopping basket' do

  let(:shopping_basket) { <<END_SHOPPING_BASKET
Quantity, Product, Price
1, book, 12.49
END_SHOPPING_BASKET
  }
  let(:shopping_basket_processor) { ShoppingBasketProcessor.new }
  let(:expected_line_item) { build(:line_item, quantity: 1, product: 'book', price: 12.49, imported: false, sales_tax: 0.0) }
  it 'creates line items for each item' do
    expect(shopping_basket_processor.process(shopping_basket)).to include(expected_line_item)
  end
end