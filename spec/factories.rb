FactoryGirl.define do
  factory :line_item do
    quantity 1
    product 'book'
    price 12.49
    sales_tax 1.50
    imported false

    initialize_with { new(attributes) }
  end

  factory :receipt do
    initialize_with { new(attributes) }
  end

end