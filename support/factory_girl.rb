require 'factory_girl'
require_relative '../spec/factories'
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
end